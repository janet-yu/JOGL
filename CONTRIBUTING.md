# Contributing

JOGL is **100% open source**, and we fully welcome contributions! You can help us by **contributing to the code**, or translating the platform in your language.

We use the following technologies: [ReactJS](https://reactjs.org/), [NextJS](https://github.com/vercel/next.js/), Typescript, Algolia, Sass, AmazonS3, Ruby, GraphQL, Elastic Search... If you have experience in one or multiple of them, your help will be much appreciated!

When contributing to this repository, please try to focus on already planned issues / known bugs:

- **List view** - View issues in list ([link here](https://gitlab.com/JOGL/JOGL/-/issues)).
- **Board view** - View a Trello-like view of the issues listed by priority and state: to do, doing, ready/review ([link here](https://gitlab.com/JOGL/JOGL/-/boards/885598)).

- API first, head to our [API documentation](https://documenter.getpostman.com/view/8688524/SWE84xMg?version=latest) for a current view of the API.
- If you want to add code, fork from the branch `develop` (from either the front-end or back-end repository) and merge request to the branch `develop`! Any request to master or staging will be automatically rejected. (see below for more detail)

**The branch develop/staging/master are present in both the frontend-v0.1 and backend-v0.1 repository**

Please note we have a code of conduct, please follow it in all your interactions with the project.

## Adding User Stories and New Features

Here is the process we would like to see. If you have ideas of User Stories (Aka new features) there are two options:

### You don't really know how to code

- Create a User Story issue card on the JOGL repo that describes what the feature is with as much detail and visual help as you can
- Ask for help in splitting it into coding items

### You know how to code

- Create a User Story issue card on the JOGL repo that describes what the feature is with as much detail and visual help as you can
- Create corresponding issue cards on the Backend and Frontend Issues boards for each part that needs to happen. (For example, you might need a new field in the API results to display a new tag on the webapp, create two issue cards in the corresponding issue board)
- Add their link to the main issue card, and reference them as accurately as you can.

## Code Stack

The code is split between two repository, Frontend (ReactJS / NextJS) and Backend (Ruby on Rails).

- Backend: https://gitlab.com/JOGL/backend-v0.1
- Frontend: https://gitlab.com/JOGL/frontend-v0.1

We use Algolia as an external search service in order to speed up the Frontend and benefit from their amazing search engine. If working on the Backend, you will need to create a Free dev account on Algolia.

## Forking the Code Stack

If you plan on helping, you might want to fork our code stack to be able to send us merge request from your end. There is three use case:

- You only work on the Frontend part, then just fork the [frontend repository](https://gitlab.com/JOGL/frontend-v0.1), and work locally. This will be the easiest.
- You only work on the Backend part, then you should fork the [JOGL stack](https://gitlab.com/JOGL/JOGL) **AND** the [Backend repo](https://gitlab.com/JOGL/backend-v0.1). If you only fork the Backend, you will have to setup your own REDIS and Postgress DB, why? We prepared it for you! If you only Fork the JOGL stack, you will run into errors due to the `backend-v0.1` repo missing from your account.
- You work on both the backend and the frontend. Then fork all three repository, that should get you up and running in no time!

## Working on the Backend? Create a free Algolia account

As per Algolia pricing, everyone can [create a free account](https://www.algolia.com/users/sign_up). (It might say it's 14 days, but as long as you don't upgrade you can keep your free account!)
Once created, you will need to setup your API keys, then follow the Backend README on how to set it up.
If all fails, contact us so that we can help and if need be provide you with some API keys.

## Working on the Frontend? Use the .env file included

In the [frontend repository](https://gitlab.com/JOGL/frontend-v0.1), you can find a `.env.exemple` file containing some dev account Algolia keys that you can use. Feel free to copy it to create a `.env.local` file. If you work on Both the backend and the frontend, we recommend you setup your own Algolia account to be able to run your tests productively.

## Code philosophy

Here is the philosophy (I know it repeats!):

1. API first, head to our [API documentation](https://documenter.getpostman.com/view/8688524/SWE84xMg?version=latest) for a current view of the API.
2. If what you need is missing from our API, then you'll have to add either it to our [backend repository](https://gitlab.com/JOGL/backend-v0.1) or [add an issue](https://gitlab.com/JOGL/JOGL/-/issues/new?issue) in the backend repository.
3. If you want to add code, fork from the branch `develop` and merge request to the branch `develop`! Any request to master or staging will be automatically rejected.
4. DRY!
5. Object Oriented thinking, if you think you should make a new component for this, then you should indeed! Modular code is the way forward!

Some technical aspect:

1. In our current stack, we use: [ReactJS](https://reactjs.org/), [NextJS](https://github.com/vercel/next.js/), Typescript, Algolia, Sass, AmazonS3, Ruby, GraphQL, Elastic Search...
2. If you need an API to test things you can point to: https://jogl-backend-dev.herokuapp.com. See all the api routes [here](https://documenter.getpostman.com/view/8688524/SWE84xMg?version=latest).
3. If you prefer to work locally, test [our dockerstack](https://gitlab.com/JOGL/JOGL).

## Pull Request Process

1. Ensure any install or build dependencies are removed before the end of the layer when doing a
   build.
2. Update the README.md with details of changes to the interface, this includes new environment
   variables, exposed ports, useful file locations and container parameters.
3. Increase the version numbers in any examples files and the README.md to the new version that this
   Pull Request would represent. The versioning scheme we use is [SemVer](http://semver.org/).
4. You may merge the Pull Request in once you have the sign-off of two other developers, or if you
   do not have permission to do that, you may request the second reviewer to merge it for you.

## Git Workflow

### General workflow

we think the **forking workflow** is more appropriate, as it allow open contributions from anyone without the need to explicitly add someone to the contributors of the reference repo. And it is the default in many open source projects.

### Branching workflow

We like the [feature branch workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow) for its simplicity. The short lived branches reduce the time spent handling conflicts manually with git.

In this workflow commits are always made in a dedicated feature branch and never in the default branch (here `develop`).
Commits are only added to the default branch by merging a feature branch into it.

## Releases

The different environments are:

| branch    | auto deployed on | purpose                                                                            | frontend link                            |
| --------- | ---------------- | ---------------------------------------------------------------------------------- | ---------------------------------------- |
| `develop` | dev              | for the dev team and contributors and to do a quick first Breaking check           | https://jogl-frontend-dev.herokuapp.com/ |
| `staging` | staging          | pre-production for QA (finding bugs or regressions, feedback on new features, ...) | http://beta.jogl.io/                     |
| `master`  | production       | live for users                                                                     | https://app.jogl.io/                     |

So, the default branch is `develop`:

- merge requests are targeted towards `develop`
- feature branches are created from `develop`
- feature branches are rebased on top of `develop`:
  - during dev
  - **required**: just before the merge request is open
  - **required**: and after the merge request has been accepted before merging the feature branch into `develop`

Deployment to staging is done by merging `develop` into `staging` and deploying to production by merging `staging` into `master`. (Restricted to the JOGL team for sanity purposes)

## Code of Conduct

### Our Pledge

In the interest of fostering an open and welcoming environment, we as
contributors and maintainers pledge to making participation in our project and
our community a harassment-free experience for everyone, regardless of age, body
size, disability, ethnicity, gender identity and expression, level of experience,
nationality, personal appearance, race, religion, or sexual identity and
orientation.

### Our Standards

Examples of behavior that contributes to creating a positive environment
include:

- Using welcoming and inclusive language
- Being respectful of differing viewpoints and experiences
- Gracefully accepting constructive criticism
- Focusing on what is best for the community
- Showing empathy towards other community members

Examples of unacceptable behavior by participants include:

- The use of sexualized language or imagery and unwelcome sexual attention or
  advances
- Trolling, insulting/derogatory comments, and personal or political attacks
- Public or private harassment
- Publishing others' private information, such as a physical or electronic
  address, without explicit permission
- Other conduct which could reasonably be considered inappropriate in a
  professional setting

### Our Responsibilities

Project maintainers are responsible for clarifying the standards of acceptable
behavior and are expected to take appropriate and fair corrective action in
response to any instances of unacceptable behavior.

Project maintainers have the right and responsibility to remove, edit, or
reject comments, commits, code, wiki edits, issues, and other contributions
that are not aligned to this Code of Conduct, or to ban temporarily or
permanently any contributor for other behaviors that they deem inappropriate,
threatening, offensive, or harmful.

### Scope

This Code of Conduct applies both within project spaces and in public spaces
when an individual is representing the project or its community. Examples of
representing a project or community include using an official project e-mail
address, posting via an official social media account, or acting as an appointed
representative at an online or offline event. Representation of a project may be
further defined and clarified by project maintainers.

### Enforcement

Instances of abusive, harassing, or otherwise unacceptable behavior may be
reported by contacting the project team. All
complaints will be reviewed and investigated and will result in a response that
is deemed necessary and appropriate to the circumstances. The project team is
obligated to maintain confidentiality with regard to the reporter of an incident.
Further details of specific enforcement policies may be posted separately.

Project maintainers who do not follow or enforce the Code of Conduct in good
faith may face temporary or permanent repercussions as determined by other
members of the project's leadership.

### Attribution

This Code of Conduct is adapted from the [Contributor Covenant][homepage], version 1.4,
available at [http://contributor-covenant.org/version/1/4][version]

[homepage]: http://contributor-covenant.org
[version]: http://contributor-covenant.org/version/1/4/

# Contributors

Thank you to all the following users who have made contributions

- [@Xqua](https://gitlab.com/xqua)
- [@LucaH](https://gitlab.com/luknl)
- [@Luis](https://gitlab.com/kaaloo1)
- [@emak](https://gitlab.com/emak)
- [@koriroys](https://gitlab.com/koriroys)
