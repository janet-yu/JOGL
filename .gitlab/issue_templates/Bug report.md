## Bug report

Please have a quick check through our [open bug issues](https://gitlab.com/JOGL/JOGL/-/issues?label_name%5B%5D=Bug) if your problem has already been reported.
If so, you can click on :thumbsup: to indicate that you have the same issue, and you can add a comment to the issue to give more detail. If not, go ahead and create this new issue.

### Describe the bug

A clear and concise description of what the bug is.

### Expected behavior

A clear and concise description of what you expected to happen.

### Screenshots

If applicable, add screenshots to help explain your problem.

### System information

- OS: [e.g. macOS, Windows]
- Browser (if applies) [e.g. chrome, safari]
- Device: [e.g. phone, tablet, desktop]

/label ~Bug
